import React from "react";
import MachineContainer from "./MachineContainer";
import { Dropdown } from 'semantic-ui-react';
import { Visualization } from "../utils/viser";
import * as Viser from "../utils/viser";

export default class VisualizationContainer extends React.Component {
  constructor(props) {
    super(props);
    this.machineName = 'Visualization';
    this.machineType = 'Visualization';
    this.defaultOptions = {
      colorScale: Viser.Visualization.options.general.colorScale.values.Jet,
      rp: {
        distanceFunction: Viser.Visualization.options.rp.distanceFunction.values.Euclidean
      }
    };
    // this.requiredParametersOptions = [
    //     { key: 'rp', value: 'rp', text: 'Recurrence Plot' },
    //     { key: 'mds', value: 'mds', text: 'Multidimensional Scaling' },
    //     { key: 'fsd', value: 'fsd', text: 'Fuzzy Symbolic Dynamics' },
    //     { key: 'pca', value: 'pca', text: 'Principal Component Analysis' }
    // ];
    this.requiredParametersOptions = Object.entries(Viser.Visualization.methods).map(([method, description]) => (
      { key: method, value: method, text: description }
    ));
    this.handleChange = this.handleChange.bind(this);
    this.state = { requiredParameters: { files: [] } };
  }

  handleChange(event) {
    const files = event.target.files;  // cannot access event.target in setState()
    this.setState((prevState) => {
      const newParameters = Object.assign(prevState.requiredParameters, { files });
      // this.props.updateMachine({ name: this.machineName, requiredParameters: newParameters });
      return { requiredParameters: newParameters };
    });
  }

  render() {
    return (
      <MachineContainer
        machineName={this.machineName}
        machineType={this.machineType}
        requiredParameters={this.state.requiredParameters}
        defaultOptions={this.defaultOptions}
        {...this.props}
      >
        <p>Select visualization method:</p>
        <Dropdown /*placeholder="Select Visualization Method"*/ fluid selection options={this.requiredParametersOptions} onChange={this.handleChange} />
      </MachineContainer>
    );
  }
}
