import React from "react";
import PlaygroundComponent from '../components/PlaygroundComponent';

export default class PlaygroundContainer extends React.Component {
  constructor(props) {
    super(props);
    // this.config = {};
  }

  render() {
    return (
      <PlaygroundComponent {...this.props} />
    );
  }
}
