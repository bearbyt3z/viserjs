import React from "react";
import OutputComponent from '../components/OutputComponent.js';

export default class OutputContainer extends React.Component {
  constructor(props) {
    super(props);
    // this.state = { outputs: this.props.outputs };
  }

  // componentWillReceiveProps(newProps) {
  //     this.setState({ outputs: newProps.outputs })
  // }
  //
  // componentDidMount() {
  //     // this.setState({ outputs: this.props.project.outputs });
  //     // this.setState({ outputs: this.props.outputs });
  //     // this.setState({ outputs: [{ method: 'plot' }] });
  //     // wyciagnac outputy z this.props.projekt albo podobnie
  //     // fetch("/my-comments.json")
  //     //     .then(res => res.json())
  //     //     .then(comments => this.setState({ comments }))
  // }

  render() {
    return (
      <OutputComponent outputs={this.props.outputs} />
    );
    // alert(JSON.stringify(this.state.outputs));
    // const outputList = this.state.outputs && this.state.outputs.map(output =>
    // const outputList = this.props.outputs && this.props.outputs.map(
    //     (output, index) => <OutputComponent output={ output } key={ index } />
    // );
    // const outputList = this.props.outputs && this.props.outputs.map((output, index) => (
    //     Array.isArray(output) ?
    //         output.map((outputPart, indexPart) => <OutputComponent output={ outputPart } key={ `${index}-${indexPart}` } />)
    //         : <OutputComponent output={ output } key={ index } />
    // ));
    // return (
    //     <div id="outputs-container">
    //         { outputList }
    //     </div>
    // );
  }
}
