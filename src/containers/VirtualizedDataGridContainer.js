import React from "react";
import * as Helpers from "../utils/helpers";
import { ScrollSync, Table, Column, Grid, MultiGrid, AutoSizer } from 'react-virtualized';
import 'react-virtualized/styles.css';
import scrollbarSize from 'dom-helpers/util/scrollbarSize';

export default class VirtualizedDataGridContainer extends React.Component {

  constructor(props) {
    super(props);
    this.renderBodyCell = this.renderBodyCell.bind(this);
    this.renderHeaderCell = this.renderHeaderCell.bind(this);
    this.renderLeftSideCell = this.renderLeftSideCell.bind(this);
    // this.rowGetter = this.rowGetter.bind(this);
    this.data = this.props.data;
    // this.data = this.props.data.map((row, rowIndex) => row.reduce((acc, cur, index) => {
    //     acc[`f${index+1}`] = cur;
    //     return acc;
    // }, { rowNumber: (rowIndex+1) }));
    // alert(JSON.stringify(this.data));
    // this.data = this.props.data.map((row, rowIndex) => {
    //     row.unshift(rowIndex+1);
    //     return row;
    // });
    // const headers = ['No.'];
    // for (let i = 1; i < this.props.data[0].length; i++)
    //     headers.push(`F#${i}`);
    // this.data.unshift(headers);
    this.headers = [];//['No.'];
    for (let i = 1; i <= this.props.data[0].length; i++)
      this.headers.push(`F#${i}`);
    this.rowNumbers = Helpers.range(1, this.props.data.length);
  }

  renderBodyCell({ columnIndex, key, rowIndex, style }) {
    const value = this.data[rowIndex][columnIndex];
    return (
      <div
        key={key}
        style={style}
        title={value}
        className="virtualized-data-grid-body-cell"
        onClick={(e) => alert(`clicked=${columnIndex},${rowIndex}`)}
      >
        {value}
      </div>
    )
  }

  renderLeftSideCell({ columnIndex, key, rowIndex, style }) {
    const value = this.rowNumbers[rowIndex];
    return (
      <div
        key={key}
        style={style}
        title={value}
        className="virtualized-data-grid-left-side-cell"
      >
        {value}
      </div>
    )
  }

  renderHeaderCell({ columnIndex, key, rowIndex, style }) {
    const value = this.headers[columnIndex];
    return (
      <div
        key={key}
        style={style}
        title={value}
        className="virtualized-data-grid-header-cell"
      >
        {value}
      </div>
    )
  }

  renderLeftHeaderCell({ columnIndex, key, rowIndex, style }) {
    return (
      <div
        key={key}
        style={style}
      // className="virtualized-data-grid-header-cell"
      >
        No.
      </div>
    )
  }

  render() {
    const rowCount = this.data.length;
    const columnCount = this.data[0].length;
    const rowHeight = 30;
    const columnWidth = 100;
    const leftSideColumnWidth = (rowCount < 100) ? 38 : ((rowCount < 1000) ? 54 : 68);
    const overscanRowCount = 5;
    const overscanColumnCount = 0;
    // TODO: https://github.com/bvaughn/react-virtualized/blob/master/source/ColumnSizer/ColumnSizer.example.js
    return (
      <ScrollSync>
        {({ clientHeight, clientWidth, onScroll, scrollHeight, scrollLeft, scrollTop, scrollWidth }) => {
          return (
            <AutoSizer>
              {({ width, height }) => {
                const bodyWidth = width - leftSideColumnWidth - scrollbarSize();
                const computedColumnWidth = (columnWidth * columnCount < bodyWidth) ? Math.floor(bodyWidth / columnCount) : columnWidth;
                return (
                  <div className="virtualized-data-grid">
                    <div>
                      <Grid
                        className={"virtualized-data-grid-left-header"}
                        style={{ lineHeight: `${rowHeight - 2}px` }}
                        cellRenderer={this.renderLeftHeaderCell}
                        height={rowHeight}
                        width={leftSideColumnWidth}
                        rowHeight={rowHeight}
                        columnWidth={leftSideColumnWidth}
                        rowCount={1}
                        columnCount={1}
                      />
                      <Grid
                        className={"virtualized-data-grid-left-side"}
                        style={{ lineHeight: `${rowHeight}px` }}
                        cellRenderer={this.renderLeftSideCell}
                        height={height - rowHeight - scrollbarSize()}
                        width={leftSideColumnWidth}
                        rowHeight={rowHeight}
                        columnWidth={leftSideColumnWidth}
                        rowCount={rowCount}
                        columnCount={1}
                        overscanRowCount={overscanRowCount}
                        overscanColumnCount={overscanColumnCount}
                        scrollTop={scrollTop}
                      />
                    </div>
                    <div>
                      <Grid
                        className={"virtualized-data-grid-header"}
                        style={{ lineHeight: `${rowHeight - 2}px` }}
                        cellRenderer={this.renderHeaderCell}
                        height={rowHeight}
                        width={width - leftSideColumnWidth - scrollbarSize()}
                        rowHeight={rowHeight}
                        columnWidth={computedColumnWidth}
                        rowCount={1}
                        columnCount={columnCount}
                        overscanRowCount={overscanRowCount}
                        overscanColumnCount={overscanColumnCount}
                        scrollLeft={scrollLeft}
                      />
                      <Grid
                        className={"virtualized-data-grid-body"}
                        style={{ lineHeight: `${rowHeight}px` }}
                        cellRenderer={this.renderBodyCell}
                        height={height - rowHeight}
                        width={width - leftSideColumnWidth}
                        rowHeight={rowHeight}
                        columnWidth={computedColumnWidth}
                        rowCount={rowCount}
                        columnCount={columnCount}
                        overscanRowCount={overscanRowCount}
                        overscanColumnCount={overscanColumnCount}
                        onScroll={onScroll}
                      />
                    </div>
                  </div>
                );
              }}
            </AutoSizer>
          );
        }}
      </ScrollSync>
    );
  }

  // rowGetter({ index }) {
  //     // return this.data[index].reduce((accumulator, current, colIndex) => {
  //     //     accumulator[`feature${colIndex+1}`] = current;
  //     //     return accumulator;
  //     // }, { rowNumber: (index+1) });
  //     const row = this.data[index].reduce((accumulator, current, colIndex) => {
  //         accumulator[`feature${colIndex+1}`] = current;
  //         return accumulator;
  //     }, { rowNumber: (index+1) });
  //     // console.log(row);
  //     return row;
  // }

  // render() {
  //     const tableWidth = 450;
  //     const tableHeight = 450;
  //     const rowCount = 500;
  //     const colCount = 30;
  //     const cellHeight = 50;
  //     const cellWidth = 72;
  //     const scrollbarWidth = 15
  //     const headers = new Array(colCount - 1).fill(0).map((val, idx) => `H${idx + 1}`); // the fixed header that only scrolls horizontally
  //     const makeRow = row => new Array(colCount - 1).fill(0).map((val, idx) => `R${row} C${idx + 1}`);
  //     const rows = new Array(rowCount).fill(0).map((val, idx) => makeRow(idx)); // the main body
  //     const fixedCol = new Array(rowCount - 1).fill(0).map((val, idx) => `R${idx + 1} C0`); // the fixed column that only scrolls vertically
  //     const fixedCell = 'R0 C0'; // The fixed cell that never moves
  //
  //     const Cell = ({
  //                       columnIndex,
  //                       key,
  //                       rowIndex,
  //                       style,
  //                   }) =>
  //         <div className='grid-cell' key={key} style={style}>
  //             {rows[rowIndex][columnIndex]}
  //         </div>
  //
  //     const HeaderCell = ({
  //                             columnIndex,
  //                             key,
  //                             style,
  //                         }) =>
  //         <div className='grid-cell' key={key} style={style}>
  //             {headers[columnIndex]}
  //         </div>
  //
  //     const FixedColCell = ({
  //                               rowIndex,
  //                               key,
  //                               style,
  //                           }) =>
  //         <div className='grid-cell' key={key} style={style}>
  //             {fixedCol[rowIndex]}
  //         </div>
  //
  //     const FixedCell = () =>
  //         <div>
  //             {fixedCell}
  //         </div>
  //
  //     return (
  //         <ScrollSync>
  //             {({ onScroll, scrollTop, scrollLeft }) =>
  //                 <div>
  //                     <div
  //                         style={{
  //                             position: 'absolute',
  //                             top: 0,
  //                             left: 0,
  //                         }}
  //                     >
  //                         <Grid
  //                             cellRenderer={FixedCell}
  //                             columnCount={1}
  //                             columnHeight={cellHeight}
  //                             columnWidth={cellWidth}
  //                             height={cellHeight}
  //                             rowCount={1}
  //                             rowHeight={cellHeight}
  //                             rowWidth={cellWidth}
  //                             width={cellWidth}
  //                         />
  //                     </div>
  //                     <div
  //                         style={{
  //                             position: 'absolute',
  //                             top: cellHeight,
  //                             left: 0,
  //                         }}
  //                     >
  //                         <Grid
  //                             cellRenderer={FixedColCell}
  //                             className={'no-scroll'}
  //                             columnCount={1}
  //                             columnHeight={rowCount * cellHeight}
  //                             columnWidth={cellWidth}
  //                             height={tableHeight}
  //                             rowCount={rowCount - 1}
  //                             rowHeight={cellHeight}
  //                             rowWidth={colCount * cellWidth}
  //                             scrollTop={scrollTop}
  //                             width={cellWidth}
  //                         />
  //                     </div>
  //                     <div
  //                         style={{
  //                             position: 'absolute',
  //                             top: 0,
  //                             left: cellWidth,
  //                         }}
  //                     >
  //                         <Grid
  //                             cellRenderer={HeaderCell}
  //                             className={'no-scroll'}
  //                             columnCount={colCount - 1}
  //                             columnHeight={rowCount * cellHeight}
  //                             columnWidth={cellWidth}
  //                             height={cellHeight}
  //                             rowCount={1}
  //                             rowHeight={cellHeight}
  //                             rowWidth={colCount * cellWidth}
  //                             scrollLeft={scrollLeft}
  //                             width={tableWidth}
  //                         />
  //                     </div>
  //                     <div
  //                         style={{
  //                             position: 'absolute',
  //                             top: cellHeight,
  //                             left: cellWidth,
  //                         }}
  //                     >
  //                         <Grid
  //                             cellRenderer={Cell}
  //                             columnCount={colCount - 1}
  //                             columnHeight={rowCount * cellHeight}
  //                             columnWidth={cellWidth}
  //                             height={tableHeight + scrollbarWidth}
  //                             onScroll={onScroll}
  //                             rowCount={rowCount - 1}
  //                             rowHeight={cellHeight}
  //                             rowWidth={colCount * cellWidth}
  //                             width={tableWidth + scrollbarWidth}
  //                         />
  //                     </div>
  //                 </div>
  //             }
  //         </ScrollSync>
  //     );
  // }

  // render() {
  //     const columnWidth = 80;
  //     const columnCount = this.props.data[0].length;
  //     // TODO: jest column sizer: https://github.com/bvaughn/react-virtualized/blob/master/source/ColumnSizer/ColumnSizer.example.js
  //     return (
  //         <AutoSizer>
  //             {({ width, height }) => {
  //                 const computedColumnWidth = (columnWidth * columnCount < width) ? Math.floor(width / columnCount) : columnWidth;
  //                 return (
  //                     <Grid
  //                         cellRenderer={ this.cellRenderer }
  //                         columnCount={ columnCount }
  //                         columnWidth={ computedColumnWidth }
  //                         height={ height }
  //                         rowCount={ this.props.data.length }
  //                         rowHeight={ 30 }
  //                         width={ width }
  //                     />
  //                 );
  //             }}
  //         </AutoSizer>
  //     );
  // }

  // render() {
  //     const columnWidth = 80;
  //     const columnCount = this.data[0].length;
  //     // TODO: jest column sizer: https://github.com/bvaughn/react-virtualized/blob/master/source/ColumnSizer/ColumnSizer.example.js
  //     // const columns = Object.keys(this.data[0]).map((key) => <Column label={ key } dataKey={ key } width={ 80 } />);
  //     const columns = this.data[0].map(
  //         (value, index) => <Column label={ `F#${index+1}` } dataKey={ `feature${index+1}` } key={ index+1 } width={ 80 } className="app-data-table-column" />
  //     );
  //     columns.unshift(<Column label="No." dataKey="rowNumber" key={ 0 } width={ 40 } className="app-data-table-first-column" />);
  //     console.log(columns);
  //     const width = 450;
  //     const height = 450;
  //     // return (
  //         {/*<AutoSizer>*/}
  //             {/*{({ width, height }) => {*/}
  //                 const computedColumnWidth = (columnWidth * columnCount < width) ? Math.floor(width / columnCount) : columnWidth;
  //                 return (
  //                     <Table
  //                         width={ width }
  //                         height={ height }
  //                         headerHeight={ 30 }
  //                         headerClassName="app-data-table-header"
  //                         rowClassName="app-data-table-row"
  //                         rowHeight={ 30 }
  //                         rowCount={ this.data.length }
  //                         rowGetter={ this.rowGetter }
  //                         // rowGetter={({ index }) => this.data[index]}
  //                         // rowGetter={ ({ index }) => {
  //                         //     const row = this.data[index].reduce((accumulator, current, colIndex) => {
  //                         //         accumulator['feature' + (colIndex+1)] = current;
  //                         //         // alert(JSON.stringify(accumulator));
  //                         //         return accumulator;
  //                         //     }, { rowNumber: (index+1) });
  //                         //     // alert(JSON.stringify(row));
  //                         //     return row;
  //                         // }}
  //                     >
  //                         {/*{ columns }*/}
  //                         <Column dataKey="rowNumber" label="No." width={ 50 } />
  //                         <Column dataKey="feature1" label="f1" width={ 300 } />
  //                         <Column dataKey="feature2" label="f2" width={ 300 } />
  //                         <Column dataKey="feature3" label="f3" width={ 300 } />
  //                         <Column dataKey="feature4" label="f4" width={ 300 } />
  //                     </Table>
  //                 );
  //         //     }}
  //         // </AutoSizer>
  //     // );
  // }
  // constructor(props) {
  //     super(props);
  //     this.cellRenderer = this.cellRenderer.bind(this);
  //     this.data = this.props.data.map((value, index) => {
  //         value.unshift(index);
  //         return value;
  //     });
  //     const headers = ['No.'];
  //     for (let i = 1; i < this.props.data[0].length; i++)
  //         headers.push(`F#${i}`);
  //     this.data.unshift(headers);
  // }
  //
  // cellRenderer ({ columnIndex, key, rowIndex, style }) {
  //     const value = this.data[rowIndex][columnIndex];
  //     return (
  //         <div
  //             key={ key }
  //             style={ style }
  //             title={ value }
  //             onClick={ (e) => alert(`clicked=${columnIndex},${rowIndex}`) }
  //         >
  //             { value }
  //         </div>
  //     )
  // }
  //
  // render() {
  //     const columnWidth = 80;
  //     const columnCount = this.data[0].length;
  //     // TODO: jest column sizer: https://github.com/bvaughn/react-virtualized/blob/master/source/ColumnSizer/ColumnSizer.example.js
  //     return (
  //         <AutoSizer>
  //             {({ width, height }) => {
  //                 const computedColumnWidth = (columnWidth * columnCount < width) ? Math.floor(width / columnCount) : columnWidth;
  //                 return (
  //                     <MultiGrid
  //                         fixedColumnCount={ 1 }
  //                         fixedRowCount={ 1 }
  //                         cellRenderer={ this.cellRenderer }
  //                         columnCount={ columnCount }
  //                         columnWidth={ computedColumnWidth }
  //                         height={ height }
  //                         rowCount={ this.data.length }
  //                         rowHeight={ 30 }
  //                         width={ width }
  //                     />
  //                 );
  //             }}
  //         </AutoSizer>
  //     );
  // }
  //
  // cellRenderer ({ columnIndex, key, rowIndex, style }) {
  //     const value = this.props.data[rowIndex][columnIndex];
  //     return (
  //         <div
  //             key={ key }
  //             style={ style }
  //             title={ value }
  //             onClick={ (e) => alert(`clicked=${columnIndex},${rowIndex}`) }
  //         >
  //             { value }
  //         </div>
  //     )
  // }
  //
  // render() {
  //     const columnWidth = 80;
  //     const columnCount = this.props.data[0].length;
  //     // TODO: jest column sizer: https://github.com/bvaughn/react-virtualized/blob/master/source/ColumnSizer/ColumnSizer.example.js
  //     return (
  //         <AutoSizer>
  //             {({ width, height }) => {
  //                 const computedColumnWidth = (columnWidth * columnCount < width) ? Math.floor(width / columnCount) : columnWidth;
  //                 return (
  //                     <Grid
  //                         cellRenderer={ this.cellRenderer }
  //                         columnCount={ columnCount }
  //                         columnWidth={ computedColumnWidth }
  //                         height={ height }
  //                         rowCount={ this.props.data.length }
  //                         rowHeight={ 30 }
  //                         width={ width }
  //                     />
  //                 );
  //             }}
  //         </AutoSizer>
  //     );
  // }
}
