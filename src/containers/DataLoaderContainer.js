import React from "react";
import MachineContainer from "./MachineContainer";
import FileInputContainer from "./FileInputContainer";

export default class DataLoaderContainer extends React.Component {
  constructor(props) {
    super(props);
    this.machineName = 'Data Loader';
    this.machineType = 'DataLoader';
    this.defaultOptions = { header: false };
    this.handleChange = this.handleChange.bind(this);
    // this.handleSettingsSave = this.handleSettingsSave.bind(this);
    // this.handleSettingsOpen = this.handleSettingsOpen.bind(this);
    // this.handleSettingsClose = this.handleSettingsClose.bind(this);
    this.state = { requiredParameters: { files: [] } };
  }
  //
  // componentDidMount() {
  //     this.props.addMachine({ name: this.machineName, type: 'DataLoader', options: { headers: false } });
  // }
  //
  // componentWillUnmount() {
  //     this.props.removeMachine({ name: this.machineName });
  // }
  //
  // handleSettingsSave() {
  //     alert('SAVE!');
  //     // TODO: save settings...
  // }
  //
  // handleSettingsOpen() {
  //     this.setState({ settingsOpen: true });
  // }
  //
  // handleSettingsClose() {
  //     this.setState({ settingsOpen: false });
  // }
  //
  // TODO: zrobic to jako parametr(y) glowny (wymagany) i dolaczac OBOK opcji
  handleChange(event) {
    const files = event.target.files;  // cannot access event.target in setState()
    this.setState((prevState) => {
      const newParameters = Object.assign(prevState.requiredParameters, { files });
      // this.props.updateMachine({ name: this.machineName, requiredParameters: newParameters });
      return { requiredParameters: newParameters };
    });
    // this.props.updateMachine({ name: this.machineName, options: { files: event.target.files, headers: false } });
    // this.props.updateProject({ machines: { machineName: 'DataLoader1', options: { files } } });
    // const machine1 = { machineName: 'DataLoader1', machineType: 'DataLoader', options: { files, headers: false } };
    // const machine2 = { machineName: 'Visualization1', machineType: 'Visualization', machineOptions: { method: 'rp', colorScale: 'Jet' } };
  }

  render() {
    return (
      <MachineContainer
        machineName={this.machineName}
        machineType={this.machineType}
        requiredParameters={this.state.requiredParameters}
        defaultOptions={this.defaultOptions}
        {...this.props}
      >
        <p>Choose .csv file to load your data:</p>
        <FileInputContainer onChange={this.handleChange} name="files[]" accept=".csv" multiple />
        {/*<br />*/}
        {/*<input onChange={ props.handleChange } type="file" id="files" name="files[]" accept=".csv" multiple />*/}
        {/*<DataLoaderComponent*/}
        {/*machineName={ this.machineName }*/}
        {/*handleChange={ this.handleChange }*/}
        {/*settingsOpen={ this.state.settingsOpen }*/}
        {/*handleSettingsSave={ this.handleSettingsSave }*/}
        {/*handleSettingsOpen={ this.handleSettingsOpen }*/}
        {/*handleSettingsClose={ this.handleSettingsClose }*/}
        {/*/>*/}
      </MachineContainer>
    );
  }
}
