import React from "react";
import FileInputComponent from '../components/FileInputComponent';

export default class FileInputContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { labelText: 'Nothing selected...' };
    this.handleFileChange = this.handleFileChange.bind(this);
  }

  handleFileChange(event) {
    const filesCount = event.target.files.length;
    let labelText, extendedText;
    if (filesCount < 2) {
      labelText = event.target.files[0].name;
      extendedText = labelText;
    } else {
      labelText = `${filesCount} files selected`;
      extendedText = event.target.files[0].name;
      for (let i = 1; i < filesCount; i++) {
        extendedText += `\n${event.target.files[i].name}`;
      }
    }
    // const labelText = (filesCount < 2) ? event.target.files[0].name : `${filesCount} files selected`;
    this.setState({ labelText, extendedText });
    this.props.onChange(event);
  }

  render() {
    const { onChange, ...rest } = this.props;
    return (
      <FileInputComponent
        labelText={this.state.labelText}
        extendedText={this.state.extendedText}
        handleFileChange={this.handleFileChange}
        {...rest}
      />
    );
  }
}
