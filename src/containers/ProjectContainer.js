import React from "react";
import * as _ from "lodash";
import * as Helpers from "../utils/helpers";
import * as Viser from "../utils/viser";

import ProjectComponent from '../components/ProjectComponent.js';

export default class ProjectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0, running: false, stopping: false, project: {}, machines: {}, connections: {}, outputs: [] };
    this.startProject = this.startProject.bind(this);
    this.stopProject = this.stopProject.bind(this);
    this.addMachine = this.addMachine.bind(this);
    this.updateMachine = this.updateMachine.bind(this);
    this.removeMachine = this.removeMachine.bind(this);
    this.handleTabChange = this.handleTabChange.bind(this);
  }

  startProject() {
    // event.preventDefault();
    // alert(JSON.stringify(this.state.project));
    if (!Helpers.isEmptyObject(this.state.project)) {
      this.state.project.start();
      return true;
    }

    const output = document.getElementById('output');
    const dl = this.state.machines['Data Loader'];
    // alert(JSON.stringify(dl));
    if (!dl.requiredParameters || !dl.requiredParameters.files || dl.requiredParameters.files.length < 1) {
      alert('Please select a file first.');
      return false;
    }
    const project = new Viser.Project();
    this.setState({ project, running: true });
    // alert(JSON.stringify(dl));
    // alert(getClassOf(dl.options.files));
    project.addMachine(dl.name, new Viser[dl.type](dl.requiredParameters.files));
    // project.addMachine(dl.name, new Viser.DataLoader(dl.options.files));
    project.addMachine('visu1', new Viser.Visualization(output, 'rp', 'Jet'));
    project.addConnection(dl.name, 'data', 'visu1');
    // project.addMachine('visu2', new Viser.Visualization(output, 'rp', 'Electric'));
    // project.addConnection(dl.name, 'data', 'visu2');
    project.addMachine('quant', new Viser.Quantification(output));
    project.addConnection('visu1', 'rp', 'quant');
    // project.init();
    project.onFinish = (outputs) => {
      this.setState({ outputs, running: false, stopping: false, activeTab: 1 });
    };
    setTimeout(function () {  // make async - potrzebne? w viserze jest setTimeout
      project.start();
    }, 100);
  }

  stopProject() {
    // throw new Error('User stop!');
    this.setState({ stopping: true, running: false });
    const { project } = this.state;
    project.stop();  // tu trzeba posprawdzac czy juz zastopowal itd => callback do visera
    this.setState({ project });  // czy to musi byc?
  }

  addMachine(machine) {
    this.setState((prevState) => {
      prevState.machines[machine.name] = machine;
      return prevState;
      // dadaj maszyne do machines jesli taka nie istnieje wpw throw error (bo to ustawiane przez komponent (sam sie rzuci przeciez
    });
  }

  updateMachine(machine) {
    // alert(JSON.stringify(machine));
    this.setState((prevState) => {
      _.mergeWith(prevState.machines[machine.name], machine, Helpers.dontMergeArrays);
      return prevState;
    });
  }

  removeMachine(machine) {
    // TODO: tu trzeba tez usunac wszystkie connections
  }

  handleTabChange(e, { activeIndex }) {
    this.setState({ activeTab: activeIndex });
  }

  render() {
    const propsToPass = {
      startProject: this.startProject,
      stopProject: this.stopProject,
      addMachine: this.addMachine,
      updateMachine: this.updateMachine,
      removeMachine: this.removeMachine,
      outputs: this.state.outputs,
      running: this.state.running,
      stopping: this.state.stopping,
      activeTab: this.state.activeTab,
      handleTabChange: this.handleTabChange
    };
    return (
      <ProjectComponent {...propsToPass} />
    );
  }
}
