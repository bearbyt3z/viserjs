import React from "react";
import * as _ from "lodash";
import MachineComponent from '../components/MachineComponent';

export default class MachineContainer extends React.Component {
  constructor(props) {
    super(props);
    // this.handleChange = this.handleChange.bind(this);
    // this.handleSettingsSave = this.handleSettingsSave.bind(this);
    this.handleSettingsOpen = this.handleSettingsOpen.bind(this);
    this.handleSettingsClose = this.handleSettingsClose.bind(this);
    this.handleSettingsChange = this.handleSettingsChange.bind(this);
    this.handleSettingsDefaults = this.handleSettingsDefaults.bind(this);
    this.state = {
      requiredParameters: Object.assign({}, (this.props.requiredParameters)),    // don't do deepClone here!
      options: _.cloneDeep(this.props.defaultOptions),    // need to clone, because they will be overwritten (an object with references)
      settingsOpen: false
    };
  }

  componentDidMount() {
    this.props.addMachine({ name: this.props.machineName, type: this.props.machineType, options: this.state.options });
  }

  componentDidUpdate(prevProps) {
    // alert('upd');   // to chyba jest ciezkie bo pare razy sie updateuje a tu robimy deep equal....
    // alert(JSON.stringify(this.props.requiredParameters));
    // alert(JSON.stringify(this.state.requiredParameters));
    // alert(JSON.stringify(prevProps.requiredParameters));
    // if (!_.isEqual(this.props.requiredParameters, prevProps.requiredParameters)) {
    if (!_.isEqual(this.props.requiredParameters, this.state.requiredParameters)) {
      this.setState({ requiredParameters: Object.assign({}, (this.props.requiredParameters)) });  // need to clone requiredParameters for if to be true (we get reference to an object)
      // alert(JSON.stringify(this.state.requiredParameters));
      this.props.updateMachine({ name: this.props.machineName, requiredParameters: this.props.requiredParameters });  // cannot be this.state.requiredParameters because state is not updated yet! (why?)
      // alert('done upd');
    }
  }

  componentWillUnmount() {
    this.props.removeMachine({ name: this.props.machineName });
  }

  handleSettingsChange(event, data) {
    // const option = event.currentTarget.getAttribute('data-setting');
    // Object.entries(data).forEach(([key, value]) => alert('key: ' + key + ', value: ' + value));
    // alert('value: ' + event.nativeEvent.target.value);
    // // alert(JSON.stringify(event.target));
    // // return function(event) {
    //     alert(option + ', ' + event.target.checked);
    // // };
    const setting = data['data-setting'];
    let settingValue;
    switch (data.type) {
      case 'checkbox':
        settingValue = data.checked;
        break;
      default:
        settingValue = data.value;
    }
    this.setState((prevState) => Object.assign(prevState.options, { [setting]: settingValue }));
  }

  handleSettingsDefaults() {
    this.setState({ options: _.cloneDeep(this.props.defaultOptions) });
  }

  // handleSettingsSave() {  // TODO: change save -> restore default
  //     alert('SAVE!');
  //     // TODO: save settings...
  //     this.state = { options: this.props.defaultOptions , settingsOpen: false };  // to samo co w constr
  //     // this.setState({ settingsOpen: false });
  // }
  //
  handleSettingsOpen() {
    this.setState({ settingsOpen: true });
  }

  handleSettingsClose() {
    this.setState({ settingsOpen: false });
  }

  // handleChange(event) {
  //     const files = event.target.files;  // cannot access event.target in setState()
  //     this.setState((prevState) => {
  //         const newOptions = Object.assign({ files }, prevState.options);
  //         this.props.updateMachine({ name: this.machineName, options: newOptions });
  //         return { options: newOptions };
  //     });
  //     // this.props.updateMachine({ name: this.machineName, options: { files: event.target.files, headers: false } });
  //     // this.props.updateProject({ machines: { machineName: 'DataLoader1', options: { files } } });
  //     // const machine1 = { machineName: 'DataLoader1', machineType: 'DataLoader', options: { files, headers: false } };
  //     // const machine2 = { machineName: 'Visualization1', machineType: 'Visualization', machineOptions: { method: 'rp', colorScale: 'Jet' } };
  // }
  //
  render() {
    return (
      <MachineComponent
        machineName={this.props.machineName}
        // handleChange={ this.handleChange }
        settingsOpen={this.state.settingsOpen}
        // handleSettingsSave={ this.handleSettingsSave }
        handleSettingsOpen={this.handleSettingsOpen}
        handleSettingsClose={this.handleSettingsClose}
        handleSettingsChange={this.handleSettingsChange}
        handleSettingsDefaults={this.handleSettingsDefaults}
        options={this.state.options}
      >
        { this.props.children}
      </MachineComponent>
    );
  }
}
