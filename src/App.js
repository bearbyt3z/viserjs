import React from 'react';
// import ReactDOM from 'react-dom';
// import classNames from 'classnames/bind';
import { Container, Menu, Button, Icon, Grid, Segment, Divider, Tab, Label, Form, Input, Checkbox, List, Modal, Header } from 'semantic-ui-react';
// import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
// import 'react-table/react-table.css';

// import logo from './logo.svg';

import ProjectContainer from './containers/ProjectContainer.js';

import styles from './stylesheets/main.scss';
// {styles.bgColor}

export default class App extends React.Component {
    render() {
        return (
            <div className="App" id="main-wrapper">
                <Container fluid>
                    <header className="app-header">
                        {/*<img src={logo} className="App-logo" alt="logo" />*/}
                        <h1 className="app-title">
                            { this.props.appName }
                        </h1>
                        <h5 className="app-subtitle">
                            {/*Future*/} Data analysis
                        </h5>
                    </header>
                    {/*<MenuContainer { ...this.props } />*/}
                    <ErrorBoundary>
                        <main>
                            <ProjectContainer/>
                        </main>
                    </ErrorBoundary>
                    {/*<div className="container-fluid">*/}
                        {/*<header className="app-header">*/}
                            {/*/!*<img src={logo} className="App-logo" alt="logo" />*!/*/}
                            {/*<h1 className="app-title">*/}
                                {/*{ this.props.appName }*/}
                            {/*</h1>*/}
                            {/*<h6 className="app-subtitle">*/}
                                {/*Future data analysis*/}
                            {/*</h6>*/}
                        {/*</header>*/}
                        {/*<p className="app-intro">*/}
                            {/*/!*Future analysis for your data*!/*/}
                        {/*</p>*/}
                        {/*<ErrorBoundary>*/}
                            {/*<main>*/}
                                {/*<ProjectContainer/>*/}
                            {/*</main>*/}
                        {/*</ErrorBoundary>*/}
                    {/*</div>*/}
                </Container>
            </div>
        );
    }
}

class MenuContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { activeItem: 'aboutUs' };
        this.handleItemClick = this.handleItemClick.bind(this);
    }

    handleItemClick(e, { name }) {
        this.setState({ activeItem: name });
    }

    render() {
        const { activeItem } = this.state;
        return (
            <Menu inverted>
                <Menu.Item
                    name='aboutUs'
                    active={activeItem === 'aboutUs'}
                    onClick={this.handleItemClick}
                />
                <Menu.Item name='jobs' active={activeItem === 'jobs'} onClick={this.handleItemClick} />
                <Menu.Item
                    name='locations'
                    active={activeItem === 'locations'}
                    onClick={this.handleItemClick}
                />
            </Menu>
        );
    }
}

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error, info) {
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        // logErrorToMyService(error, info);
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h3>Something went wrong.</h3>;
        }
        return this.props.children;
    }
}

// function DataLoaderComponent(props) {
//     return (
//         <DraggablePanel header="Data Loader" >
//             <Modal basic size="tiny" /*centered={ false }*/ closeIcon onClose={ props.handleSettingsClose } open={ props.settingsOpen } closeOnDimmerClick={ true } >
//                 {/*<Modal.Header>Settings: { props.machineName }</Modal.Header>*/}
//                 <Header as='h2' icon inverted>
//                     <Icon name='settings' />
//                     {/*Settings: { props.machineName }*/}
//                     Machine Settings
//                     {/*<Header.Subheader>Manage settings of { props.machineName } machine.</Header.Subheader>*/}
//                 </Header>
//                 <Modal.Content>
//                     <Modal.Description>
//                         <Form inverted>
//                             <Form.Field>
//                                 <label>First Name</label>
//                                 <input placeholder='First Name' />
//                             </Form.Field>
//                             <Form.Field>
//                                 <label>Last Name</label>
//                                 <input placeholder='Last Name' />
//                             </Form.Field>
//                             <Form.Field>
//                                 <Checkbox label='I agree to the Terms and Conditions' />
//                             </Form.Field>
//                             {/*<Button type='submit'>Submit</Button>*/}
//                         </Form>
//                     </Modal.Description>
//                 </Modal.Content>
//                 <Modal.Actions>
//                     {/*<Button negative icon="cancel" labelPosition="left" content="Cancel" onClick={ props.handleSettingsClose } />*/}
//                     <Button color="blue" icon="save" content="Save" onClick={ props.handleSettingsSave } />
//                 </Modal.Actions>
//             </Modal>
//             <Button onClick={ props.handleSettingsOpen } secondary icon="cog" className="machine-settings-button" />
//             <p>Choose .csv file to load your data:</p>
//             <FileInput onChange={ props.handleChange } name="files[]" accept=".csv" multiple />
//             {/*<br />*/}
//             {/*<input onChange={ props.handleChange } type="file" id="files" name="files[]" accept=".csv" multiple />*/}
//         </DraggablePanel>
//     );
// }
