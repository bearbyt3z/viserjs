import * as Papa from 'papaparse/papaparse.min.js';
import * as Plotly from 'plotly.js';
import * as _ from 'lodash';
import * as Helpers from './helpers.js';

// window.onerror = function (msg, url, lineNo, columnNo, error) {
//     var string = msg.toLowerCase();
//     var substring = "script error";
//     if (string.indexOf(substring) > -1){
//         alert('Script Error: See Browser Console for Detail');
//     } else {
//         var message = [
//             'Message: ' + msg,
//             'URL: ' + url,
//             'Line: ' + lineNo,
//             'Column: ' + columnNo,
//             'Error object: ' + JSON.stringify(error)
//         ].join(' - ');
//
//         alert(message);
//     }
//
//     return false;
// };
// window.addEventListener("error", function (e) {
//     alert("Error occurred: " + e.error.message);
//     return false;
// });
// window.addEventListener('unhandledrejection', function (e) {
//     alert("Error occurred: " + e.reason.message);
// });

// window.onerror = function(msg, url, line, col, error) {
//     alert(`${msg}\n At ${line}:${col} of ${url}`);
//     alert(`Stack:\n\n ${error.stack}`);
// };

// Classes definition

class Project {
  constructor() {
    this.startDate = new Date();
    this.machines = {};
    this.waitingMachines = [];
    this._outputs = [];
    this._stop = false;
    this._initDone = false;
    // this.connections = {};
  }

  addMachine(newName, newMachine) {
    this.machines[newName] = { object: newMachine, name: newName };
  }

  addConnection(outputMachineName, outputName, inputMachineName) {
    // if ()
    //     throw new TypeError('ProjectError: Different type of connection ends');
    this.machines[inputMachineName].inputs = { [outputName]: outputMachineName };  // TODO: nie mozna tu cale inputy nadpisywac!!!
  }

  get outputs() {
    this._outputs = [];
    Object.values(this.machines).forEach((machine) => {  // values => machine.outputs
      // alert('get outputs machine.outputs: ' + JSON.stringify(machine.object));
      if (!Helpers.isEmptyObject(machine.object.outputs))
        Object.entries(machine.object.outputs).forEach(([outputName, outputValue]) => {
          if (outputValue.ready)
            this._outputs.push(outputValue);  // TODO: jesli tylko value to zamieniecnic na values
        });
    });
    // alert('get outputs: ' + JSON.stringify(this._outputs));
    return this._outputs;
  }

  init() {
    Object.values(this.machines).forEach((machine) => this.waitingMachines.push(machine));
  }

  stop() {
    this._stop = true;
  }

  start() {
    if (!this._initDone) {
      this.init();
      this._initDone = true;
    }
    this._stop = false;
    this._mainLoop();
  }

  _mainLoop() {
    if (!this._stop && !Helpers.isEmptyArray(this.waitingMachines)) {
      // for (let waitingCount = this.waitingMachines.length; waitingCount > 0; waitingCount--) {
      const machine = this.waitingMachines.shift();
      if (!this.runMachineIfReady(machine))
        this.waitingMachines.push(machine);
      setTimeout(() => this._mainLoop(), 100);
      // // alert(JSON.stringify(machine));
      // if (!machine.inputs || Helpers.isEmptyObject(machine.inputs)) {
      //     machine.object.onready = () => this.start();
      //     machine.object.run();
      // }
      // else {
      //     if (!this.runMachineIfReady(machine))
      //         this.waitingMachines.push(machine);
      //     // // alert('with inputs start')
      //     // const inputsReady = Object.entries(machine.inputs).every(
      //     //     ([inputName, outputMachineName]) => this.machines[outputMachineName].object.ready
      //     // );
      //     // // alert(inputsReady)
      //     // if (inputsReady) {
      //     //     // TU TRZEBA BINDOWAC OUTPUTY DO INPUTY BO NIE SA ZBINDOWANE PRZECIEZ
      //     //     // POPRAWIC OUTPUTS w MAchine!!!!!!!!!!!!
      //     //     Object.entries(machine.inputs).forEach(([inputName, outputMachineName]) => {
      //     //         machine.object.inputs[inputName] = this.machines[outputMachineName].object.outputs[inputName];
      //     //     });
      //     //     await machine.object.run();
      //     //     alert('with inputs done')
      //     // }
      //     // else
      //     //     this.waitingMachines.push(machine);
      // }
    }
    else {
      this.onFinish(this.outputs);
    }
  }

  runMachineIfReady(machine) {
    // alert(machine.name)
    const noInputs = !machine.inputs || Helpers.isEmptyObject(machine.inputs);
    const inputsReady = noInputs || Object.entries(machine.inputs).every(
      ([inputName, outputMachineName]) => this.machines[outputMachineName].object.outputs[inputName].ready
    );
    if (inputsReady) {
      // binding outputs to inputs:
      noInputs || Object.entries(machine.inputs).forEach(([inputName, outputMachineName]) => {
        machine.object.inputs[inputName] = this.machines[outputMachineName].object.outputs[inputName];
      });
      // machine.object.onready = () => this.start(); // ?????????????????????
      machine.object.run();
      return true;
    }
    return false;
  }
}

// Machine class
class Machine {

  constructor(inputs = {}, outputs = {}) {
    // inputs, outputs are in the form:
    // input = { name: { method: 'inputType', ready: false } }
    // output = { name: { method: 'outputType', value: 'outputValue' } }
    // when input will be ready => ready will be set to true
    this.inputs = inputs;
    this.outputs = outputs;

    // this.ready = false;
    // this._outputsPromise = new Promise((resolve, reject) => {
    //     // alert(this instanceof DataLoader)
    //     this.onready = () => resolve(this._outputs);
    //     this.run();
    // });
  }

  // get outputs() {
  //     return new Promise((resolve, reject) => {
  //         this.onready = () => resolve(this._outputs);
  //         this.run();
  //     });
  // }
  // get outputs() {
  //     return this._outputsPromise;
  // }

  // get outputs() {
  //     return this._outputs;
  // }
  // set outputs(value) {
  //     this._outputs = value;
  // }
  // get inputs() {
  //     return this._inputs;
  //     // return (async (inputs) => {
  //     //     return await inputs;
  //     // })(this._inputs);
  // }
  // set inputs(value) {
  //     this._inputs = value;
  // }

  // in run() method use "await this.inputs" to be sure inputs are ready
  // run() method should call this.onready() method when it's done
  // machine should add ready=true property to completed output...
  run() {
    throw new Error('ViserMachineError: Machine have to implement run() method');
  }

  // onready() {
  //     throw new Error('ViserMachineError: onready() method is not defined');
  // }
}

class DataLoader extends Machine {

  // constructor(files, { header = false }) {
  constructor(files) {
    if (!(files instanceof FileList) || (files.length < 1))
      throw new Error('ViserDataLoaderError: Illegal files object passed to DataLoader constructor');
    super(null);
    this.files = files;
    this.filesLoaded = [];
    this.data = [];
    // this.outputs = new Promise((resolve, reject) => {
    //     this.onload = () => { resolve({data : this.data}); };
    //     this.loadFiles();
    // });
    this.outputs = { data: { type: 'dataTable', ready: false, entries: this.data } };
  }

  run() {

    const promises = [];
    for (const file of this.files) {

      if (!file.type.match('text/csv')) // some error message should be send here...
        continue;

      if (this.filesLoaded.includes(file.name))
        continue;

      // try {
      //     const values = await readFileAsText(file);
      //     this.data.push({file: file.name, values});
      //     document.getElementById('output').innerHTML += JSON.stringify(this.outputs) + '<br><br>++++++++++++++++++++++<br><br>';
      //     alert(JSON.stringify(this.outputs))
      // } catch (error) {
      //     console.warn(error.message);
      // }

      promises.push(new Promise((resolve, reject) => {
        const reader = new FileReader();
        (fileName => {
          reader.onload = event => {
            // const values = Helpers.arrayToNumbers($.csv.toArrays(event.target.result));
            const values = Papa.parse(event.target.result, {
              // delimiter: "",	// auto-detect
              // newline: "",	// auto-detect
              // quoteChar: '"',
              // escapeChar: '"',
              // header: false,
              // trimHeaders: false,
              dynamicTyping: true,
              // preview: 0,
              // encoding: "",
              // worker: false,
              // comments: false,
              // step: undefined,
              // complete: undefined,
              // error: undefined,
              // download: false,
              skipEmptyLines: true
              // chunk: undefined,
              // fastMode: undefined,
              // beforeFirstChunk: undefined,
              // withCredentials: undefined,
              // transform: undefined
            });
            this.data.push({ name: fileName, values: values.data });
            this.filesLoaded.push(fileName);
            // const array = parseCSV(event.target.result);
            // document.getElementById('output').innerHTML += JSON.stringify(this.data) + '<br><br>++++++++++++++++++++++<br><br>';
            resolve(` ${fileName} file was successfully loaded and parsed`);  // space at the begging!
          };
          reader.onerror = () => {
            reject(new Error(`ViserDataLoaderError: Unable to read file: ${fileName}`));
          };
        })(file.name);
        reader.readAsText(file);
      }));
    }
    Promise.all(promises).then(values => {
      console.log(`ViserDataLoader:${values}`);  // no space!
      this.outputs.data.ready = true;  // ?????????????
      // this.ready = true;
      // this.onready();
    }).catch(error => { alert(error); });    // TODO: alert error???????
    return true;
  }
}

class Visualization extends Machine {

  static get types() {
    return ['rp', 'fsd', 'mds', 'pca'];
  }

  static get options() {
    return Object.freeze({
      general: {
        colorScale: {
          values: {
            Jet: 'Jet',
            Electric: 'Electric'
          },
          default: 'Jet',
          name: 'Color scale',
          description: 'Color scale used in the plot (as a distance value or time progress)'
        }
      },
      rp: {
        epsilon: {
          values: [0, Infinity],
          default: 1,
          name: 'Epsilon threshold',
          description: 'Epsilon threshold for B&W plots'
        },
        distanceFunction: {
          values: {
            Euclidean: 'Euclidean',
            Cosine: 'Cosine'
          },
          default: 'Euclidean',
          name: 'Distance function',
          description: 'Similarity function of two data points'
        },
        distanceLimitMinimum: {
          values: [0, Infinity],
          default: 0,
          name: 'Distance limit (minimum)',
          description: 'Limit the minimal possible distance between two data points'
        },
        contrast: {
          description: 'Enhance the contrast of the recurrence plot'
        }
      }
    });
  }

  static get methods() {
    return Object.freeze({
      RP: 'Recurrence Plot',
      MDS: 'Multidimensional Scaling',
      FSD: 'Fuzzy Symbolic Dynamics',
      PCA: 'Principal Component Analysis'
    });
  }

  static get colorScales() {
    return Object.freeze({
      Jet: 0,
      Electric: 1,
      Hot: 2,
      RdBu: 3,
      Bluered: 4,
      Blackbody: 5,
      Portland: 6,
      Earth: 7,
      Picnic: 8,
      YIGnBu: 9,
      YIOrRd: 10,
      Greens: 11,
      Greys: 12
    });
  }

  static get colorscales() {
    return ['Jet', 'Electric', 'Hot', 'RdBu', 'Bluered', 'Blackbody', 'Portland', 'Earth', 'Picnic', 'YIGnBu', 'YIOrRd', 'Greens', 'Greys'];
  }

  static get defaultPlotLayout() {
    const bgcolor = 'rgba(16, 16, 16, 0.8)';
    const fgcolor = '#efefef';
    const axisLayout = {
      // mirror: true,
      linecolor: bgcolor,  // hide lines
      tickcolor: bgcolor   // & tick lines
    };
    return {
      autosize: false,
      width: 450,
      height: 450,
      // margin: {
      //     l: 50,
      //     r: 50,
      //     b: 100,
      //     t: 100,
      //     pad: 4
      // },
      plot_bgcolor: bgcolor,
      paper_bgcolor: bgcolor,
      xaxis: axisLayout,
      yaxis: axisLayout,
      font: {
        color: fgcolor
      },
      title: 'Visualization Plot',
      titleNoteSize: '75%'
    };
  }

  constructor(container, method = 'rp', colorScale = 'Jet') {
    // if (!(container instanceof Element))
    //     throw new Error('ViserVisualizationError: Illegal container passed to constructor');
    if (!Visualization.types.includes(method))
      throw new Error('ViserVisualizationError: Illegal visualization method passed to constructor');
    if (!Visualization.colorscales.includes(colorScale))
      throw new Error('ViserVisualizationError: Illegal colorScale passed to constructor');
    super();
    this.inputs = { data: { type: 'data' } };
    this.outputs = {
      'plot': { type: 'plot', ready: false },
      [method]: { type: 'dataTable', ready: false }    // TODO: type dataTable? is it true for all methods???
    };
    // this.container = container;
    this.colorScale = colorScale;
    // this.figures = [];
    this.method = method;
    switch (this.method) {
      case 'rp':
        this.methodHandler = Visualization.recurrencePlot;
        // this.methodOptions = {
        // }
        break;
      case 'mds':
        this.methodHandler = Visualization.recurrencePlot;  // TODO: change to ...
        break;
      default:
        this.methodHandler = null;  // TODO: null?
    }
  }

  run() {
    // const inputs = await this.inputs;
    // alert(JSON.stringify(this.inputs.data));
    this.outputs[this.method].entries = [];
    this.outputs.plot.entries = [];
    for (const data of this.inputs.data.entries) {
      // const figure = addDraggablePanel(this.container, 'FIGURE');
      // const figure = new DraggablePanel(this.container, 'FIGURE');
      // this.outputs.rp = Visualization.recurrencePlot(data.values, figure.element, this.colorScale, data.name);
      const result = this.methodHandler(data.values, this.container, this.colorScale, data.name);
      this.outputs[this.method].entries.push({ name: data.name, values: result[this.method] });
      this.outputs.plot.entries.push({ name: data.name, values: result.plot });  // TODO: value == plot props? or entire plot? => plot props I think... (entire plot should return component...)
      // this.figures.push(figure);
      console.log(`ViserVisualization: Recurrence Plot for ${data.name} file was successfully generated`);  // TODO: generated?
    }
    this.outputs[this.method].ready = true;
    this.outputs.plot.ready = true;
    return true;
  }

  static recurrencePlot(data, container, colorScale = 'Jet', titleNote = '') {
    const dataLength = data.length;
    const rp = distanceMatrix(data);
    // const rp = math.zeros(dataLength, dataLength);
    // let row = [];
    // rp = ML.distanceMatrix(data, ML.euclidean);
    // let subtract;
    // for (let i = 0; i < dataLength - 1; i++) {  // => lepiej bedzie wykorzystac math.js?
    //     for (let j = i + 1; j < dataLength; j++) {
    //         subtract = math.subtract(data[i], data[j]);
    //         rp.set([i, j], math.norm(subtract));
    //     }
    // }
    const axisRangeData = generateTicks(1, dataLength, dataLength);  // we want [1, dataLength] range, not default: [0, dataLength-1]
    const plotData = [{
      // z: rp.toArray(),
      colorbar: {
        outlinecolor: 'black'
      },
      x: axisRangeData,
      y: axisRangeData,
      z: rp,
      colorscale: colorScale,
      type: 'heatmap'
    }];
    const title = 'Recurrence Plot' + (Helpers.isEmptyString(titleNote) ? '' :
      `<br><span style="font-size: ${Visualization.defaultPlotLayout.titleNoteSize};">(${titleNote})</span>`); // doesn't work with classes: <span class="plotly-title-smaller">
    const tickvals = generateTicks(0, dataLength, 6);
    const axisLayout = {
      tickvals,
      range: [0, dataLength]  // add 0 or dataLength to the plot range (we have [0, dataLength-1] or [1, dataLength] from data)
    };
    let layout = _.mergeWith({}, Visualization.defaultPlotLayout, {
      title,
      // annotations: ['abc', 'xyz'],  // ??????????????????????
      xaxis: axisLayout,
      yaxis: axisLayout
    }, Helpers.dontMergeArrays);
    // Plotly.newPlot(container, plotData, layout);
    return { rp, plot: { data: plotData, layout } };
  }
}

class Quantification extends Machine {

  constructor(container, type = 'rp', colorscale = 'Jet') {
    // if (!(container instanceof Element))
    //     throw new Error('ViserQuantificationError: Illegal container passed to constructor');
    super();
    // this.container = container;
    this.inputs = { rp: { type: 'dataTable' } };
    this.outputs = { measures: { type: 'keyValueMap', ready: false } };  // TODO: keyValueMap ???  a nie po prostu map???
  }

  run() {
    this.outputs.measures.entries = [];
    // const measures = new Map();
    // measures.set('Recurrence Rate', Quantification.recurrenceRate(this.inputs.rp.value));
    // measures.set('Probability of Recurrence', Quantification.probabilityOfRecurrence(this.inputs.rp.value));
    for (const rpEntry of this.inputs.rp.entries) {
      const measures = [];
      measures.push(['Recurrence Rate', Quantification.recurrenceRate(rpEntry.values)]);
      const por = Quantification.probabilityOfRecurrence(rpEntry.values);
      measures.push(['Probability of Recurrence', por.por]);
      this.outputs.measures.entries.push({ name: rpEntry.name, values: measures });
      // const panel = new DraggablePanel(this.container);
      // panel.addKeyValuePair('Recurrence Rate', Quantification.recurrenceRate(this.inputs.rp));
      // const por = Quantification.probabilityOfRecurrence(this.inputs.rp);
      // panel.addKeyValuePair('Probability of Recurrence', por.por);
    }
    this.outputs.measures.ready = true;
  }

  static recurrenceRate(rp) {
    const rpSize = rp.length;
    rp = Helpers.normalization(rp);
    let rr = 0;
    for (let i = 0; i < rpSize; i++) {
      for (let j = 0; j < rpSize; j++) {
        rr += rp[i][j];
      }
    }
    return 1 - rr / (rpSize * rpSize);  // 1 - RR => colored RP plots have reversed scale (0 means it's close, not 1 like in black-white RP)
  }

  static probabilityOfRecurrence(rp, sigma = 1) {
    const rpSize = rp.length;
    rp = Helpers.normalization(rp);
    const normFactor = 1;// / (sigma * Math.sqrt(2 * Math.PI));
    let pori = [];
    for (let i = 0; i < rpSize; i++) {
      pori[i] = 0;
      for (let j = 0; j < rpSize; j++) {
        const arg = rp[i][j] / sigma;
        pori[i] += normFactor * Math.exp(- (arg * arg) / 2);
      }
    }
    pori = multiplyArrayByScalar(pori, 1 / rpSize);
    const por = vectorSum(pori) / rpSize;
    return { por, pori };
  }
}


// GUI start

// class Panel {
//     constructor(container, elementType = 'DIV', size = { width: 500, height: 500}) {
//         this.container = container;
//         this.element = document.createElement(elementType);
//         this.element.style.width = `${size.width}px`;
//         this.element.style.height = `${size.height}px`;
//         this.element.classList.add('viser-panel');
//         this.container.appendChild(this.element);
//     }
//
//     addKeyValuePair(label, value) {
//         // if (!this.table) {
//         //     this.table = document.createElement('TABLE');
//         //     this.container.appendChild(this.table);
//         // }
//         // const newRow = document.createElement('TR');
//         // const keyCell = document.createElement('TD');
//         // const valueCell = document.createElement('TD');
//         const wrapper = document.createElement('DIV');
//         wrapper.classList.add('label-with-value', 'panel', 'panel-default', 'align-items-center', 'justify-content-center');
//         const textNode = document.createTextNode(`${label}: ${value}`);
//         wrapper.appendChild(textNode);
//         this.element.appendChild(wrapper);
//     }
// }
//
// class DraggablePanel extends Panel {
//     constructor(container, elementType = 'DIV') {
//         super(container, elementType);
//         this.element.classList.add('draggable');
//         $(this.element).draggable({
//             // start: moveOnTop,
//             snap: true,
//             opacity: 0.7,
//             cancel: '.draglayer',
//             // grid: [20, 20],
//             // containment: 'document',
//             stack: '.draggable'
//         }).click(function () {
//             $(this).css('z-index', 100);
//             $(this).siblings().css('z-index', 50);
//         });
//     }
// }

// function addDraggablePanel(container, method = 'DIV') {
//     const panel = document.createElement(method);
//     panel.classList.add('panel-draggable');
//     $(panel).draggable({
//         // start: moveOnTop,
//         snap: true,
//         opacity: 0.7,
//         cancel: '.draglayer',
//         // grid: [20, 20],
//         // containment: 'document',
//         stack: '.panel-draggable'
//     }).click(moveOnTop);
//     container.appendChild(panel);
//     return panel;
// }


// HELPER START HERE

function distanceMatrix(data) {
  const dataLength = data.length;
  const dm = Helpers.filledArray(dataLength, dataLength, 0);  // TODO: sprawdzic czasy bez filledArray dm[i][i] = 0;
  for (let i = 0, norm; i < dataLength - 1; i++) {
    for (let j = i + 1; j < dataLength; j++) {
      norm = vectorNorm(vectorSubtract(data[i], data[j]));
      dm[i][j] = norm;
      dm[j][i] = norm;
    }
  }
  return dm;
}

function generateTicks(start, stop, count) {
  const result = Helpers.range(start, stop, (stop - start) / (count - 1));
  return result.map(x => Math.round(x));
}


const multiplyArrayByScalar = (array, scalar) => array.map(x => x * scalar);
const addScalarToArray = (array, scalar) => array.map(x => x + scalar);

const vectorSubtract = (v1, v2) => v1.map((x, i) => x - v2[i]);

const vectorNorm = (vector) => Math.sqrt(vector.reduce((accumulator, value) => accumulator + value * value, 0));
const vectorSum = (vector) => vector.reduce((accumulator, value) => accumulator + value, 0);

// function readFileAsText(inputFile) {
//     const tmpFileReader = new FileReader();
//     return new Promise((resolve, reject) => {
//         tmpFileReader.onload = event => {
//             const values = $.csv.toArrays(event.target.result);
//             resolve(values);
//         };
//         tmpFileReader.onerror = () => {
//             tmpFileReader.abort(); // ???? wywalic chyba bo Thrown when abort is called while no read operation is in progress (that is, the state isn't LOADING).
//             reject(new DOMException(`Unable to read file: ${inputFile.name}`));
//         };
//         tmpFileReader.readAsText(inputFile);
//     });
// }


// function parseCSV(str) {
//     var arr = [];
//     var quote = false;  // true means we're inside a quoted field
//
//     // iterate over each character, keep track of current row and column (of the returned array)
//     for (var row = col = c = 0; c < str.length; c++) {
//         var cc = str[c], nc = str[c+1];        // current character, next character
//         arr[row] = arr[row] || [];             // create a new row if necessary
//         arr[row][col] = arr[row][col] || '';   // create a new column (start with empty string) if necessary
//
//         // If the current character is a quotation mark, and we're inside a
//         // quoted field, and the next character is also a quotation mark,
//         // add a quotation mark to the current column and skip the next character
//         if (cc == '"' && quote && nc == '"') { arr[row][col] += cc; ++c; continue; }
//
//         // If it's just one quotation mark, begin/end quoted field
//         if (cc == '"') { quote = !quote; continue; }
//
//         // If it's a comma and we're not in a quoted field, move on to the next column
//         if (cc == ',' && !quote) { ++col; continue; }
//
//         // If it's a newline (CRLF) and we're not in a quoted field, skip the next character
//         // and move on to the next row and move to column 0 of that new row
//         if (cc == '\r' && nc == '\n' && !quote) { ++row; col = 0; ++c; continue; }
//
//         // If it's a newline (LF or CR) and we're not in a quoted field,
//         // move on to the next row and move to column 0 of that new row
//         if (cc == '\n' && !quote) { ++row; col = 0; continue; }
//         if (cc == '\r' && !quote) { ++row; col = 0; continue; }
//
//         // Otherwise, append the current character to the current column
//         arr[row][col] += cc;
//     }
//     return arr;
// }


// function isAPIAvailable() {
//   // Check for the various File API support.
//   if (window.File && window.FileReader && window.FileList && window.Blob) {
//     // Great success! All the File APIs are supported.
//     return true;
//   } else {
//     // source: File API availability - http://caniuse.com/#feat=fileapi
//     // source: <output> availability - http://html5doctor.com/the-output-element/
//     document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
//     // 6.0 File API & 13.0 <output>
//     document.writeln(' - Google Chrome: 13.0 or later<br />');
//     // 3.6 File API & 6.0 <output>
//     document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
//     // 10.0 File API & 10.0 <output>
//     document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
//     // ? File API & 5.1 <output>
//     document.writeln(' - Safari: Not supported<br />');
//     // ? File API & 9.2 <output>
//     document.writeln(' - Opera: Not supported');
//     return false;
//   }
// }

// exports:
export { Project, DataLoader, Visualization, Quantification };
