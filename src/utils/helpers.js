
/* Array functions */
export const dontMergeArrays = (dst, src) => { if (Array.isArray(dst)) return src; };

export const filledArray = (rows, columns, value = 0) => Array(rows).fill().map(() => Array(columns).fill(value));
export const arrayColumn = (array, column) => array.map(x => x[column]);
export const arrayToNumbers = (array) => array.map(x => (Array.isArray(x) ? arrayToNumbers(x) : Number(x)));
export const arrayToFixed = (array, digits) => array.map(x => (Array.isArray(x) ? arrayToFixed(x, digits) : Number(x.toFixed(digits))));

export const range = (start, stop, step = 1) => {
  const result = [start];
  for (let i = start + step; i <= stop; i += step)
    result.push(i);
  return result;
};

/* Variable Checkers */
export const getClassOf = Function.prototype.call.bind(Object.prototype.toString);

export const isEmptyArray = (array) => !Array.isArray(array) || !array.length;
export const isEmptyObject = (object) => object == null || typeof object !== 'object' || Object.keys(object).length === 0;
// export const isEmptyObject = (object) => object == null || object.constructor !== Object || Object.keys(object).length === 0;
export const isEmptyString = (string) => typeof string !== 'string' && !(string instanceof String) || string.trim().length === 0;


/* Data Processing */
/**
 * Performs normalization of a data array. Normalization is performed on a data parameter itself (no new array is generated).
 * @param {Array<Array<number>>} data - Array to normalize
 * @param {Array<number>} [range = [0, 1]] - Array with the minimum and maximum value
 * @returns {Array} reference to normalized array (the same as data parameter)
 */
export const normalization = (data, range = [0, 1]) => {
  // console.time('normalization');
  const rangeLength = range[1] - range[0];
  const rows = data.length;
  const cols = data[0].length;
  const normData = filledArray(rows, cols, 0);
  for (let j = 0; j < cols; j++) {
    const column = arrayColumn(data, j);
    const min = Math.min(...column);
    const max = Math.max(...column);
    const diffMaxMin = max - min;
    for (let i = 0; i < rows; i++) {
      normData[i][j] = (diffMaxMin !== 0) ? range[0] + rangeLength * (column[i] - min) / diffMaxMin : min;
      // data[i][j] = (diffmaxmin === 0) ? min : range[0] + rangeLength * (column[i] - min) / diffmaxmin;
    }
  }
  // console.timeEnd('normalization');
  return normData;
  // return data;
};
