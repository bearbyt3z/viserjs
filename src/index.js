// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import App from './App';

// import registerServiceWorker from './registerServiceWorker';

const appName = 'Viser';

ReactDOM.render(
    <App appName={ appName } />,
    document.getElementById('root')
);
// registerServiceWorker();

module.hot.accept();
