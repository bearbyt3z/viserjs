import React from "react";
import { Button, Icon } from 'semantic-ui-react';

export default function ControlsComponent(props) {
  return (
    <div id="controls-container">
      <Button.Group size="tiny" >
        <Button secondary animated="vertical" onClick={props.startProject} loading={props.running} title="Start Project" className="control-button" >
          <Button.Content visible>
            <Icon name="play" />
          </Button.Content>
          <Button.Content hidden>
            <Icon name="play" />
          </Button.Content>
        </Button>
        <Button secondary animated="vertical" onClick={props.stopProject} disabled={!props.running} loading={props.stopping} title="Stop Project" className="control-button" >
          <Button.Content visible>
            <Icon name="stop" />
          </Button.Content>
          <Button.Content hidden>
            <Icon name="stop" />
          </Button.Content>
        </Button>
        <Button secondary animated="vertical" onClick={() => { window.location = window.location; }} disabled={props.running || props.stopping} title="Clear Everything" className="control-button" >
          <Button.Content visible>
            <Icon name="ban" />
          </Button.Content>
          <Button.Content hidden>
            <Icon name="ban" />
          </Button.Content>
        </Button>
      </Button.Group>
    </div>
  );
}
