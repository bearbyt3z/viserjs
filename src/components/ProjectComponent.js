import React from "react";
import { Tab } from 'semantic-ui-react';
import OutputContainer from '../containers/OutputContainer.js';
import PlaygroundContainer from '../containers/PlaygroundContainer.js';

export default function ProjectComponent(props) {
  const panes = [{
    menuItem: { key: 'project', content: 'Project', icon: /*'sitemap'*/ 'random' },
    pane: (
      <Tab.Pane key="project" inverted textAlign="center" >
        {/*<Segment raised inverted textAlign="center">*/}
        <PlaygroundContainer {...props} />
        {/*</Segment>*/}
      </Tab.Pane>
    )
  }, {
    menuItem: { key: 'results', content: 'Results', icon: 'chart area' },
    pane: (
      <Tab.Pane key="results" inverted textAlign="center" style={{ borderTop: '1px solid #555' }} >  {/* top border is present only on the first pane! */}
        {/*<Segment raised inverted textAlign="center" >*/}
        <OutputContainer outputs={props.outputs} />
        {/*</Segment>*/}
      </Tab.Pane>
    )
  }];
  return (
    <div id="project-container">
      <Tab
        activeIndex={props.activeTab}
        onTabChange={props.handleTabChange}
        renderActiveOnly={false}
        menu={{ inverted: true, secondary: true }}
        panes={panes}
      />
    </div>
  );
}
