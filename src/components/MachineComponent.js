import React from "react";
import { Button, Icon, Form, Checkbox, Select, Modal, Header, Input, Table, Grid } from 'semantic-ui-react';
import DraggablePanel from './DraggablePanel';

export default function MachineComponent(props) {
  // const options = Object.entries(props.options).map(([key, value]) => (
  //     <Grid.Row key={ key }>
  //         <Grid.Column textAlign="center">{ key }</Grid.Column>
  //         <Grid.Column textAlign="center"><Checkbox toggle defaultChecked={ value } onChange={ props.handleSettingsChange } data-option={ key } /></Grid.Column>
  //     </Grid.Row>
  // ));
  const options = Object.entries(props.options).map(([key, value]) => {
    let optionElement;
    // alert('[' + key + ', ' + value + ']: ' + typeof value);
    switch (typeof value) {
      case 'boolean':
        optionElement = <Checkbox toggle checked={value} onChange={props.handleSettingsChange} data-setting={key} />;
        break;
      case 'string':
        optionElement = <input type="text" />;
        break;
    }
    return (
      <Table.Row key={key}>
        <Table.Cell>{key}</Table.Cell>
        <Table.Cell>{optionElement}</Table.Cell>
        {/*<Table.Cell><Select placeholder='Select your country' onChange={ props.handleSettingsChange } data-setting={ key } options={[{ key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' }]} /></Table.Cell>*/}
      </Table.Row>
    );
  });
  return (
    <React.Fragment>
      <Modal basic size="tiny" centered={true} closeIcon onClose={props.handleSettingsClose} open={props.settingsOpen} closeOnDimmerClick={true} >
        {/*<Modal.Header>Settings: { props.machineName }</Modal.Header>*/}
        <Header as="h2" icon inverted>
          <Icon name="settings" />
          {/*Settings: { props.machineName }*/}
          Machine Settings
          {/*<Header.Subheader>Manage settings of { props.machineName } machine.</Header.Subheader>*/}
        </Header>
        <Modal.Content>
          <Modal.Description>
            {/*<Grid columns='equal' inverted padded divided>*/}
            {/*{ options }*/}
            {/*</Grid>*/}
            <Table inverted celled textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Parameter</Table.HeaderCell>
                  <Table.HeaderCell>Value</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {options}
              </Table.Body>
            </Table>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          {/*<Button negative icon="cancel" labelPosition="left" content="Cancel" onClick={ props.handleSettingsClose } />*/}
          {/*<Button color="blue" icon="save" content="Save" onClick={ props.handleSettingsSave } />*/}
          <Button color="blue" icon="undo alternate" content="Defaults" onClick={props.handleSettingsDefaults} />
        </Modal.Actions>
      </Modal>
      <DraggablePanel header={props.machineName} className="machine-panel" >
        <Button onClick={props.handleSettingsOpen} secondary icon="cog" className="machine-settings-button" />
        {props.children}
      </DraggablePanel>
    </React.Fragment>
  );
}
