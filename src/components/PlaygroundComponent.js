import React from "react";
import ControlsComponent from './ControlsComponent';
import DataLoaderContainer from '../containers/DataLoaderContainer';
import VisualizationContainer from '../containers/VisualizationContainer';

//TODO: Playground -> Design ???
export default function PlaygroundComponent(props) {
  return (
    <div id="playground-container">
      <ControlsComponent {...props} />
      <DataLoaderContainer {...props} />
      <VisualizationContainer {...props} />
    </div>
  );
}
