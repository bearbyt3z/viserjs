import React from "react";
import Plot from 'react-plotly.js';
import 'react-table/react-table.css';
// import ReactTable from 'react-table';
import { Table } from 'semantic-ui-react';
import DraggablePanel from './DraggablePanel.js';
import VirtualizedDataGridContainer from '../containers/VirtualizedDataGridContainer.js';
import * as Helpers from "../utils/helpers";
import { Quantification, Visualization } from "../utils/viser";

export default function OutputComponent(props) {
  const outputElements = [];
  props.outputs.forEach((output) => {
    // const output = props.output;
    // if (Helpers.isEmptyObject(output) || output.type === 'dataTable') return;    // nothing to render
    if (Helpers.isEmptyObject(output)) return;    // nothing to render

    switch (output.type) {
      case 'plot':
        output.entries.forEach((entry) => outputElements.push(<Plot data={entry.values.data} layout={entry.values.layout} />));
        break;
      case 'dataTable':
        // TODO: extract stats from data to show as output (instead of data itself?)
        if (!Helpers.isEmptyArray(output.entries[0].values))
          outputElements.push(<VirtualizedDataGridContainer data={Helpers.arrayToFixed(output.entries[0].values, 5)} />);
        break;
      case 'keyValueMap':
        output.entries.forEach((entry) => outputElements.push(
          <React.Fragment>
            <header style={{ paddingTop: '10px' }} >
              <h3>Quantification<br />
                <span style={{ fontSize: '75%', fontWeight: 'normal' }}>
                  ({entry.name})
                </span>
              </h3>
            </header>
            <Table celled padded inverted selectable textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Measure</Table.HeaderCell>
                  <Table.HeaderCell>Value</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {entry.values.map(([measure, value]) => (
                  <Table.Row key={measure} >
                    <Table.Cell>{measure}</Table.Cell>
                    <Table.Cell>{Number(value.toFixed(5))}</Table.Cell>
                  </Table.Row>
                  // Number() => to remove unnecessary zeros
                ))}
              </Table.Body>
            </Table>
            {/*<ReactTable*/}
            {/*data={ value.values }*/}
            {/*columns={ columns }*/}
            {/*showPagination={ true }*/}
            {/*defaultPageSize={ 5 }*/}
            {/*className="-striped -highlight"*/}
            {/*style={{ width: "450px", height: "350px" }}*/}
            {/*/>*/}
          </React.Fragment>
        ));
        break;
      // default:
      //     return (null);
    }
  });
  const outputsList = outputElements.map((element, index) => (
    <DraggablePanel className="output-panel" bounds="#outputs-container" key={index}>
      {element}
    </DraggablePanel>
  ));
  return (
    <div id="outputs-container">
      {outputsList}
    </div>
  );
  // return (
  //     <DraggablePanel className="output-panel" bounds="#outputs-container" >
  //         { outputElement }
  //     </DraggablePanel>
  // );
}

// export default function OutputComponent(props) {
//     const output = props.output;
//     if (Helpers.isEmptyObject(output) || output.type === 'dataTable') return (null);    // nothing to render
//     // if (Helpers.isEmptyObject(output)) return (null);    // nothing to render
//
//     let outputElement;
//     // alert(JSON.stringify(output.type));
//     switch (output.type) {
//         case 'plot':
//             outputElement = <Plot data={ output.value.data } layout={ output.value.layout } />;
//             break;
//         case 'dataTable':
//             // Za długo wypluwa tabele => trzeba ja zrobic pod przycisk, a z tabeli wypluc tylko staty
//             // break;
//             if (!Helpers.isEmptyArray(output.value[0].values[0])) {
//                 // alert(JSON.stringify(output.value[0].values[0]));
//                 // const columns = output.value[0].values[0].map((value, index) => ({
//                 //     Header: `F#${index+1}`, accessor: d => d[index], id: `feature${index+1}` //, Cell: this.renderEditable
//                 // }));
//                 // columns.unshift({
//                 //     Header: 'No.', accessor: d => d.index, id: 'row', maxWidth: 50, filterable: false,// className: 'text-center',
//                 //     Cell: (row) => <span>{row.index + 1}</span>
//                 // });
//                 // alert(JSON.stringify(columns));
//                 // outputElement = <ReactTable
//                 //     data={ output.value[0].values }
//                 //     columns={ columns }
//                 //     showPagination={ false }
//                 //     defaultPageSize={ output.value[0].values.length }
//                 //     className="-striped -highlight"
//                 //     style={{
//                 //         width: "450px",
//                 //         height: "450px"
//                 //     }}
//                 // />;
//                 outputElement = <VirtualizedDataGridContainer data={ output.value[0].values } />
//             }
//             break;
//         case 'keyValueMap':
//             const columns = [{
//                 Header: 'Key', accessor: d => d[0], id: 'key'
//             }, {
//                 Header: 'Value', accessor: d => d[1], id: 'value'
//             }];
//             outputElement = <ReactTable
//                 data={ output.value }
//                 columns={ columns }
//                 showPagination={ true }
//                 defaultPageSize={ 10 }
//                 className="-striped -highlight"
//                 style={{
//                     width: "450px",
//                     height: "450px"
//                 }}
//             />;
//             break;
//         default:
//             return (null);
//     }
//     return (
//         <DraggablePanel className="output-panel" bounds="#outputs-container" >
//             { outputElement }
//         </DraggablePanel>
//     );
// }
