import React from "react";
import classNames from "classnames/bind";
import Draggable from 'react-draggable';
// import { Header } from 'semantic-ui-react';

export default function DraggablePanel(props) {
  const className = classNames('draggable-panel', props.className);
  // const header = props.header && <Header inverted as="h3">{ props.header }</Header>;
  const header = props.header && <header><h3>{props.header}</h3></header>;
  return (
    <Draggable
      defaultClassName={className}
      // defaultClassNameDragging: string
      // defaultClassNameDragged: string
      cancel="input, .draglayer, .modebar"  // TODO: move this up to caller...
      bounds={props.bounds}
    >
      <div>
        {header}
        {props.children}
      </div>
    </Draggable>
  );
}
