import React from "react";
import { Button, Label, Icon } from 'semantic-ui-react';

export default function FileInputComponent(props) {
  const { labelText, extendedText, handleFileChange, ...rest } = props;
  const fileInput = <input type="file" hidden onChange={handleFileChange} {...rest} />;
  const selectButtonWidth = '90px';
  return (
    <Button fluid as='label' labelPosition='left'>
      <Label style={{ width: '100%' }} title={extendedText}>
        <Icon name='file' />
        {labelText}
      </Label>
      <Button as="label" color="blue" style={{ width: selectButtonWidth }} title="Click to select data files">
        Select {fileInput}
      </Button>
    </Button>
  );
}
